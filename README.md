# Projeto – Gustavo Notaro - Vortx

# Repositórios do projeto:

- Aplicação:
    - Backend: [https://gitlab.com/teste-vortx/aplicacao/backend](https://gitlab.com/teste-vortx/aplicacao/backend)
    - Frontend: [https://gitlab.com/teste-vortx/aplicacao/frontend](https://gitlab.com/teste-vortx/aplicacao/frontend)
- Infraestrutura:
    - EKS: [https://gitlab.com/teste-vortx/infra/eks](https://gitlab.com/teste-vortx/infra/eks)
    - Database: [https://gitlab.com/teste-vortx/infra/database](https://gitlab.com/teste-vortx/infra/database)

# Tecnologias e ferramentas usadas:

- Docker
- Helm
- Terraform
- Kubernetes
- Gitlab

# Pipeline do projeto gerenciados pelo CI/CD do Gitlab

- Build do backend através de um Dockerfile
- Build do frontend através de um Dockerfile
- Push das imagens no DockerHub
- Build do cluster EKS na AWS através do Terraform
- Deploy do banco de dados no cluster EKS com o Terraform e HELM
- Deploy do backend e frontend no cluster EKS com o HELM
